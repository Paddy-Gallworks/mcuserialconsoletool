#ifndef XMODEM_ARDUINO_INTERFACE
#define XMODEM_ARDUINO_INTERFACE

#include "PayloadIO.h" // Included for access to Payload Read and Write Functions
#include "lazydragonfly/base64.h" // For Encode and Decode!
#include "xmodem/XModem.h" // To access XModemHardwareInterface Class
#include <fstream> // For writing and reading image and art files.
#include <unistd.h>
#include <iostream>
#include <algorithm> // For use of min function

#include <chrono> // Added for better delay functionality.
#include <sys/time.h>
#include <ctime>

class XModemArduinoInterface : public XModemHardwareInterface {
  //DATA
public:
  std::ofstream outputDataFile; // Output -> Writing -> Receiving Image File
  std::ifstream inputDataFile; // Input -> Reading -> Sending Artwork
  long xModemPacketCounter = 0;
  long xModemFileBytesToRead = 0;
  unsigned char memBuffer[96];
  //FUNCTIONS
public:
  XModemArduinoInterface();
  //Interface Definition
  int recvChar(int _timeOut) override;
  void sendChar(char _char) override;
  bool rDataHandler(unsigned long , char *, int ) override;
  bool tDataHandler(unsigned long , char *, int ) override;
};

#endif //XMODEM_ARDUINO_INTERFACE
