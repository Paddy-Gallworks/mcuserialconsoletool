#ifndef PAYLOAD_IO_H
#define PAYLOAD_IO_H

#include <iostream>
#include <string>
#include <unistd.h>
#include "Serial/wiringSerial.h"


// --------------------------------------
// Payload IO Functions
// --------------------------------------
extern int deviceNumber; // Global Variable for the Payload File Descriptor
int setupPort(char* _device, int _baud);
void reportFromCommand(std::string _message);
// Define Interfaces for Instructions to Use
void writeToPayload(std::string _message);
void writeToPayload(char _char);
// Reads Single Byte from Serial Port - Does not timeout!
bool readFromPayload(char * _byte);
void flushSerial();
// ------------------------------------
// Interpret Functions
// ------------------------------------

struct payloadResponse {
  std::string message;
  int duration;
};

std::string formatResponse(char _byte);

bool checkAcknowledge(payloadResponse _reponse, char _cmd);

/* aggregateResponse
 *
 * This function joins all the characters received from the payload within the timeout or until
 * the max character limit is reached.
 *
 */
payloadResponse aggregateResponse(unsigned long timeout, int maxCharCount);
#endif //PAYLOAD_IO_H
