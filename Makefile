# Main Build
mcuConsole: main.o wiringSerial.o LazyDragonfly.o wiringSerial.o XModem.o XModemArduinoInterface.o InstructionSet.o PayloadIO.o
	g++ -o payloadConsole main.o LazyDragonfly.o wiringSerial.o base64.o XModem.o XModemArduinoInterface.o InstructionSet.o PayloadIO.o


main.o: main.cpp
	g++ -c main.cpp

# Components of the LazyDragonFly Library
LazyDragonfly.o: lazydragonfly/LazyDragonfly.cpp lazydragonfly/base64.cpp
	g++ -c lazydragonfly/LazyDragonfly.cpp
	g++ -c lazydragonfly/base64.cpp

#consoleInterface.o: ConsoleInterface.cpp
#	g++ -c ConsoleInterface.cpp

XModemArduinoInterface.o: XModemArduinoInterface.cpp
	g++ -c XModemArduinoInterface.cpp

# 'Hard' Import of Wiring Pi Source
wiringSerial.o: Serial/wiringSerial.c
	g++ -c Serial/wiringSerial.c

# Submodule of XModem
XModem.o: xmodem/XModem.cpp
	g++ -c xmodem/XModem.cpp

# Instruction Set
InstructionSet.o: InstructionSet.cpp
	g++ -c InstructionSet.cpp

# Payload IO Global FUNCTIONS
PayloadIO.o: PayloadIO.cpp PayloadIO.h
	g++ -c PayloadIO.cpp

# Clean
clean:
	rm *.o
