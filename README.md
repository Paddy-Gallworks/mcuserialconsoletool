# Payload MCU Serial Test Tool

## Description
The goal of this project is to create a program to test the Northern Images Mission Payload and Yukon Territorial Payload as part of the Canadian CubeSat project.

## Installation

The version presented here is intended for a RaspberryPi based single board computer with WiringPi install. There is no support for other environemtns at the moment.  

```
git clone <repo>
make
sudo ./payloadConsole <devID>
```

## Usage

- Arduino-CLI can be very useful in finding the device name of the attached MCU. If arduino-cli is installed and configured to program the mcu, then the following command can be used to identify the device ID or port:

```
sudo arduino-cli board list
```

- Once the payload console has started, command can be sent by typing them after the console prompt '>>'. The console will display status messages that relate to the command.

### Program Sections

The following sections will report while a command is running:
- [cmd] : Reporting from inside the InstructionSet.
- [agg] : Listening for a reponse from the MCU.
- [dec] : Decoding and Encoding data from and to Based64.

## Roadmap
Quick Todo:
- [x] Change BAUD to 56700. 
- [x] Update the file length decode to 8 bytes.
- [x] Add the instantaneous eNIM to the telemetry decode. 
- [x] Change the telemetry decode to 72 bytes
- [x] Test the two byte command rejection!
 
Long Term:
[] Add scheduler that is triggered by a command line argument and that runs a set of tasks based on a schedule in a text file.
[] Reorganize includes to improve compilation efficiency.

## Contributing

TBD

## Authors and acknowledgment
Aurora College and Yukon University CubeSat Teams.

## License

TBD

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
