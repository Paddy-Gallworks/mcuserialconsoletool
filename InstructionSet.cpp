
#include "InstructionSet.h"
#include "Logger.h"

std::string fileSendLogFile = "timing/fileSend.txt";
std::string sendDirLogFile = "timing/sendDir.txt";
std::string heartbeatLogFile =  "timing/heartbeat.txt";
std::string telemetryLogFile = "timing/telemetry.txt";
std::string captureLogFile = "timing/captureImage.txt";

void appendToFile(std::string _filename, std::string _message) {
  std::ofstream outputFile;
  outputFile.open(_filename, std::ios_base::app);
  outputFile << _message << std::endl;
  outputFile.close();
}


// List All Instruction Implementation
listCommandsInstruction::listCommandsInstruction() : LazyDragonflyCommandInterface("list") {}
std::string listCommandsInstruction::toString() {
  std::string usage = "";
  usage += "\n** PAYLOAD TEST CONSOLE USAGE **\n";
  usage += "\t- list: This command\n";
  usage += "\t- flush: Clears the contents of both send and recieve serial buffers on the PC\n";
  usage += "\t- heartbeat: Asks payload for a heartbeat check\n";
  usage += "\t- telemetry: Asks payload for up to date telemetry data\n";
  usage += "\t- fileRequest: Ask the payload for an image file\n";
  usage += "\t- fileSend: Send an artwork file to the payload\n";
  usage += "\t- sendLog: Sends the logfile contents\n";
  usage += "\t- trimLog: Clears the contents of the payload logfile\n";
  usage += "\t- checkFlags: Asks the payload for system flags\n";
  usage += "\t- captureImage: Ask Payload to capture a picture of artwork\n";
  usage += "\t- ackDown: Acknowledge the reception of Image\n";
  usage += "\t- sendDir: Sends the current positions of the file pointers\n";
  return usage;
}
void listCommandsInstruction::execute() {
  std::cout << toString() << std::endl;
}

// Flush Serial Buffers Implementation
flushInstruction::flushInstruction() : LazyDragonflyCommandInterface("flush") {}
std::string flushInstruction::toString() {
	return "Flush Instruction";
}
void flushInstruction::execute() {
	flushSerial();
}

// Heartbeat Implementation
heartbeatInstruction::heartbeatInstruction() : LazyDragonflyCommandInterface("heartbeat") {}
std::string heartbeatInstruction::toString() {
  return "Heartbeat Command";
}
void heartbeatInstruction::execute() {
  std::string _heartbeatMessage = "hhh";
  reportFromCommand("Sending Heartbeat!");
  writeToPayload(_heartbeatMessage);
  payloadResponse commandAcknowledge = aggregateResponse(3000,2); //2000 milli seconds and 2 byte expected.
  appendToFile(heartbeatLogFile, "hhh, " + to_string(commandAcknowledge.duration));
  std::cout << "[cmd] Payload Response: " << commandAcknowledge.message << std::endl;
  char* payRes = &(commandAcknowledge.message[0]);
  if((int)payRes[0] == 104 && (int)payRes[1] == 6 && (int)payRes[2] == 6) {
    std::cout << "[cmd] Response Correct -- PASS --" << std::endl;
  }
}

// Version Implementation

versionInstruction::versionInstruction() : LazyDragonflyCommandInterface("version") {}
std::string versionInstruction::toString() {
  return "Payload Version Instruction";
}
void versionInstruction::execute() {
  std::cout << "[cmd] Asking payload version" << std::endl;
  writeToPayload("vvv");
  payloadResponse commandAcknowledge = aggregateResponse(3000,2);
  payloadResponse versionString = aggregateResponse(3000,7);
  payloadResponse completionAcknowledge = aggregateResponse(3000,2);
  std::cout << "[cmd] Payload Version Response: " << versionString.message << std::endl;
}

// Telemetry Request Implementation

telemetryInstruction::telemetryInstruction() : LazyDragonflyCommandInterface("telemetry") {}
std::string telemetryInstruction::toString() {
    return "Telemetry Request Instruction";
}
void telemetryInstruction::execute() {
    std::cout << "[cmd] Sending Telemetry Request to Payload" << std::endl;
    writeToPayload("ttt");
    if(!checkAcknowledge(aggregateResponse(3000,2),'t')) { // Check command acknowledge
      return;
    }
    // Read in telemetry data
    payloadResponse payloadDataString = aggregateResponse(20000, 64); // Encoded Telemetry packet is 72 bytes long.
    appendToFile(telemetryLogFile, "gather, " + to_string(payloadDataString.duration));
    unsigned char* payloadData = (unsigned char*)&payloadDataString.message[0];
    // Base64 Decode!!
    unsigned char decodedBuffer[48];
    int outputLength = decode_base64(&payloadData[0], 64, decodedBuffer);
    // Decode Temperatures
    int16_t* temps[4];
    for(int i =0;i<4;i++) {
        temps[i] =(int16_t*) &decodedBuffer[i*2];
    }
    // Check Temperature values
    for(int i = 0; i<4; i++) {
        if(*temps[i] == -50) {
            std::cout << "[dec] Suspect no sensor on channel: " << i << " as value is -50C" << std::endl;
        }
        else if(*temps[i] > 15 && *temps[i] < 25) {
            std::cout << "[dec] Temp Ch: " << i << " in nominal range: " << *temps[i] << "C" << std::endl;
        }
        else {
            std::cout << "[dec] Temp Ch: " << i << " outside range: " << *temps[i] << "C" << std::endl;
        }
    }
    // Decode eNIM Values
    int16_t* eNIM[4];
    for(int i = 0 ; i<4; i++){
        eNIM[i] = (int16_t*) &decodedBuffer[(i*2)+16];
        std::cout << "[dec] eNIM " << i << ": " << *eNIM[i] << " LUX" << std::endl;
    }
    // Decode Memory Values
    int16_t* RAMfree;
    RAMfree = (int16_t*) &decodedBuffer[32];
    std::cout << "[dec] RAM Bytes Free: " << *RAMfree << std::endl;
    int16_t* nextExistingArt;
    int16_t* nextBlankArt;
    int16_t* nextExistingImg;
    int16_t* nextBlankImg;
    nextExistingArt = (int16_t*) &decodedBuffer[34];
    nextBlankArt = (int16_t*) &decodedBuffer[36];
    nextExistingImg = (int16_t*) &decodedBuffer[38];
    nextBlankImg = (int16_t*) &decodedBuffer[40];
    std::cout << "[dec] Next Existing Art Number: " << *nextExistingArt << std::endl;
    std::cout << "[dec] Next Blank Art Number: " << *nextBlankArt << std::endl;
    std::cout << "[dec] Next Existing Img Number: " << *nextExistingImg << std::endl;
    std::cout << "[dec] Next Blank Img Number: " << *nextBlankImg << std::endl;
    // Report Total response
    std::cout << "[cmd] Total Payload Response: " << payloadData << std::endl;
}

// File Request (Recieve) Implementation ( -- PAY to OBC -- 'fff' on the Payload)

fileRequestInstruction::fileRequestInstruction() : LazyDragonflyCommandInterface("fileRequest") {}
std::string fileRequestInstruction::toString() {
    return "File Request Instruction";
}
void fileRequestInstruction::execute() {
  // Ask Payload for File Name - 'lc' - Asks payload for the name of the image filename that will be downloaded
  std::cout << "[cmd] Requesting Filename from Payload" << std::endl;
  writeToPayload("lllccc");
  if(!checkAcknowledge(aggregateResponse(10000,2),'l')) { // Check command acknowledge
    return;
  }
  payloadResponse fileName = aggregateResponse(40000,11); // ALl filenames should be 11 bytes long, plus two for the <ack>. e.g. IMG0001.JPG
  std::cout << "[cmd] Filename Received: " << fileName.message  << std::endl;
  // Open the Local File - Optional, use the file name requested from the payload
  payloadInterface.outputDataFile.open(fileName.message);
	payloadInterface.xModemPacketCounter = 0;
  // Ask for the File download to start
  std::cout << "[cmd] Requesing file from Payload" << std::endl;
  writeToPayload("fff");
  if(!checkAcknowledge(aggregateResponse(3000,2),'f')) { // Check command acknowledge
    return;
  }
	std::cout << "[cmd] Listening for Error Acknowledge..." << std::endl;
  if(checkAcknowledge(aggregateResponse(10000,2),'f')) { // Check error acknowledge
    //payloadResponse fileLengthString = aggregateResponse(6000,8);
    //unsigned char fileLengthCharacters[4];
    //int decodedLength = decode_base64((unsigned char*)&fileLengthString.message[0], 8, fileLengthCharacters);
    //int16_t* fileLength = (int16_t*)&fileLengthCharacters;
		//std::cout << "[cmd] File Length Received: " << *fileLength << std::endl;
		std::cout << "[cmd] Starting XMod Receive" << std::endl;
		sleep(2);
		xmod.receive();
		std::cout << "[cmd] XMod Complete" << std::endl;
  }
  else {
    std::cout << "[cmd] Payload may be reporting filename error" << std::endl;
    return;
  }
  // Close Local File
  payloadInterface.outputDataFile.close();
  std::cout << "[cmd] Receive Complete, File Closed" << std::endl;
  checkAcknowledge(aggregateResponse(10000,2),'f');
}

// File Send Implementation ( -- OBC to PAY --  'aaa' on the Payload)
fileSendInstruction::fileSendInstruction() : LazyDragonflyCommandInterface("fileSend") {}
std::string fileSendInstruction::toString() {
    return "File Send Instruction";
}
void fileSendInstruction::execute() {
  // Ask the payload what it will call the file:
  std::cout << "[cmd] Asking payload for filename it will use" << std::endl;
  writeToPayload("lllbbb");
  if(!checkAcknowledge(aggregateResponse(6000,2),'l')) { // Check command acknowledge
    return;
  }
  payloadResponse filename = aggregateResponse(60000,11);
  std::cout << "[cmd] Payload Filename: " << filename.message << std::endl;
  // Select a file from the local disk
  std::string sendFilename = "ColorTest.BMP";
  std::cout << "[cmd] Local Filename: " << sendFilename << std::endl;
  // Open local file for sending
  payloadInterface.inputDataFile.open(sendFilename);
  // get length of file:
  payloadInterface.inputDataFile.seekg(0, payloadInterface.inputDataFile.end);
  payloadInterface.xModemFileBytesToRead = payloadInterface.inputDataFile.tellg();
  payloadInterface.inputDataFile.seekg(0);
  std::cout << "[cmd] Local File Length = " << payloadInterface.xModemFileBytesToRead << " bytes" << std::endl;
  if(!payloadInterface.inputDataFile.is_open()) {
    std::cout << "[cmd] Cannot Open File" << std::endl;
    return;
  }
  // Send aaa
  std::cout << "[cmd] Sending File to Payload" << std::endl;
  writeToPayload("aaa");
  // Receive Acknowledge
  if(checkAcknowledge(aggregateResponse(3000,2),'a')) { // Check command acknowledge
    payloadResponse errorAcknowledge = aggregateResponse(20000,2);
    if(checkAcknowledge(errorAcknowledge,'a')) {
      std::cout << "[cmd] Starting XModem Send" << std::endl;
      sleep(2);
      xmod.transmit();
      std::cout << "[cmd] XMod Complete" << std::endl;
    }
    else {
      return;
    }

  }
  else {
    return;
  }
  // Close Local File
  payloadInterface.inputDataFile.close();
  // Clear Trailing <ack> Characters
  checkAcknowledge(aggregateResponse(100000,2),'a'); // Completion Ack
  flushSerial();
}

// Capture Image captureImage Instruction
captureImageInstruction::captureImageInstruction() : LazyDragonflyCommandInterface("captureImage") {}
std::string captureImageInstruction::toString() {
  return "Capture Image for NIM Instruction";
}
void captureImageInstruction::execute() {
  std::cout << "[cmd] Asking payload to capture image of artwork" << std::endl;
  writeToPayload("ccc");
  // Command Acknowledge
  if(!checkAcknowledge(aggregateResponse(8000,2),'c')) { // Check command acknowledge
    return;
  }
  // Error Acknowledge
  payloadResponse errorAcknowledge = aggregateResponse(60000,2);
  if(!checkAcknowledge(errorAcknowledge,'c')) { // Check command acknowledge
    return;
  }
  else {
    appendToFile(captureLogFile, "errorAck, " + to_string(errorAcknowledge.duration));
  }
  // Complete Acknowledge
  payloadResponse completeAcknowledge = aggregateResponse(180000,2);
  if(!checkAcknowledge(completeAcknowledge,'c')) { // Check command acknowledge
    return;
  }
  else {
    appendToFile(captureLogFile, "compAck, " + to_string(completeAcknowledge.duration));
  }
}

// Acknowledge Downlink Instruction Implementation
ackDownInstruction::ackDownInstruction() : LazyDragonflyCommandInterface("ackDown") {}
std::string ackDownInstruction::toString() {
	return "Acknowledge Downlink";
}
void ackDownInstruction::execute() {
	// Send Ack Downlink
	std::cout << "[cmd] Send Acknowledge of Downlink" << std::endl;
	writeToPayload("ggg");
	// Command Acknowledge
  if(!checkAcknowledge(aggregateResponse(10000,2),'g')) { // Check command acknowledge
    return;
  }
  // Completion Acknowledge
  if(!checkAcknowledge(aggregateResponse(60000,2),'g')) { // Check command acknowledge
    return;
  }
}

// Check System Flags 'checkFlags' Instruction
checkFlagsInstruction::checkFlagsInstruction() : LazyDragonflyCommandInterface("checkFlags") {}
std::string checkFlagsInstruction::toString() {
	return "Check System Flags";
}
void checkFlagsInstruction::execute() {
	// Send Check SD Flag 'kkksss'
	std::cout << "[cmd] Asking SD Card Status" << std::endl;
	writeToPayload("kkksss");
	payloadResponse sdStatus = aggregateResponse(3000,3);
  payloadResponse completeAcknowledge = aggregateResponse(3000,2);
	std::cout << "[cmd] Payload Responded: " << sdStatus.message << std::endl;
	// Send Check Camera Flag 'kkkccc'
	std::cout << "[cmd] Asking Camera Status" << std::endl;
	writeToPayload("kkkccc");
	payloadResponse camStatus = aggregateResponse(8000,3);
  completeAcknowledge = aggregateResponse(6000,2);
	std::cout << "[cmd] Payload Responded: " << camStatus.message << std::endl;
}


// Send Log File to OBC Implementation
sendLogInstruction::sendLogInstruction() : LazyDragonflyCommandInterface("sendLog") {}
std::string sendLogInstruction::toString() {
	return "Send Log File";
}
void sendLogInstruction::execute() {
  time_t now = time(0);
	std::string fileName = "log.txt";
	// Request Log File
	std::cout << "[cmd] Requesting Log File" << std::endl;
  // Open the Local File - Optional, use the file name requested from the payload
  payloadInterface.outputDataFile.open(fileName);
	payloadInterface.xModemPacketCounter = 0;
	writeToPayload("lllggg");
  if(checkAcknowledge(aggregateResponse(3000,2),'l')) { // Check Command Acknowledge
    if(checkAcknowledge(aggregateResponse(3000,2),'l')) { // Check for Error Acknowledge
      //payloadResponse fileLengthString = aggregateResponse(10000,8);
      //unsigned char fileLengthCharacters[4];
      //int decodedLength = decode_base64((unsigned char*)&fileLengthString.message[0], 8, fileLengthCharacters);
      //uint32_t* fileLength = (uint32_t*)&fileLengthCharacters;
      //std::cout << "[cmd] File Length Received: " << std::dec << *fileLength << std::endl;
      std::cout << "[cmd] Starting XMod Receive" << std::endl;
      sleep(2);
      xmod.receive();
      checkAcknowledge(aggregateResponse(3000,2),'l');
    }
    else {
      std::cout << "[cmd] Payload may be reporting filename error, or No SD Card" << std::endl;
    }
  }
  else {
    std::cout << "[cmd] Payload does not recognize command!!" << std::endl;
    return;
  }
  // Close Local File
  payloadInterface.outputDataFile.close();
  std::cout << "[cmd] Receive Complete, File Closed" << std::endl;
}

// Trim Log File Implementation
trimLogInstruction::trimLogInstruction() : LazyDragonflyCommandInterface("trimLog") {}
std::string trimLogInstruction::toString() {
	return "Trim Log File";
}
void trimLogInstruction::execute() {
	std::cout << "[cmd] Requesting Trim of Log File" << std::endl;
	writeToPayload("llliii");
  // Command Acknowledge
  if(!checkAcknowledge(aggregateResponse(10000,2),'l')) { // Check command acknowledge
    return;
  }
  std::cout << "[cmd] Waiting for completion acknowledge" << std::endl;
  // Complete Acknowledge
  if(!checkAcknowledge(aggregateResponse(10000,2),'l')) { // Check command acknowledge
    return;
  }
}
// Send Dir File
sendDirInstruction::sendDirInstruction() : LazyDragonflyCommandInterface("sendDir") {}
std::string sendDirInstruction::toString() {
	return "Send Directory File";
}
void sendDirInstruction::execute() {
	std::string fileName = "dir.txt";
	// Request Log File
	std::cout << "[cmd] Requesting File Buffer Locations:" << std::endl;
	std::cout << "[cmd] First Existing Artwork: " << std::endl;
	writeToPayload("lllaaa");
	payloadResponse firstArtwork = aggregateResponse(60000,13);
	std::cout << firstArtwork.message << std::endl;
  appendToFile(sendDirLogFile, "nextArt, " + to_string(firstArtwork.duration) + ", " +firstArtwork.message);
	std::cout << "[cmd] First Blank Space for New Artwork: " << std::endl;
	writeToPayload("lllbbb");
	payloadResponse nextArtwork = aggregateResponse(60000,13);
	std::cout << nextArtwork.message << std::endl;
  appendToFile(sendDirLogFile, "blankArt, " + to_string(nextArtwork.duration) + ", " + nextArtwork.message);
	std::cout << "[cmd] Next Image for Downlink: " << std::endl;
	writeToPayload("lllccc");
	payloadResponse nextImage = aggregateResponse(60000,13);
	std::cout << nextImage.message << std::endl;
  appendToFile(sendDirLogFile, "nextImg, " + to_string(nextImage.duration) + ", " + nextImage.message);
	std::cout << "[cmd] Next Image for Capture: " << std::endl;
	writeToPayload("lllddd");
	payloadResponse nextCapture = aggregateResponse(60000,13);
	std::cout << nextCapture.message << std::endl;
  appendToFile(sendDirLogFile, "blankImg, " + to_string(nextCapture.duration) + ", " + nextCapture.message);
	std::cout << "[cmd] ART:" << firstArtwork.message << "-" << nextArtwork.message << " IMG:" << nextImage.message << "-" << nextCapture.message << std::endl;
}

// clear SD "clearSD" Instruction


clearSDInstruction::clearSDInstruction() : LazyDragonflyCommandInterface("clearSD") {}
std::string clearSDInstruction::toString() {
  return "Clear SD Card Contents";
}
void clearSDInstruction::execute() {
  std::cout << "[cmd] Requesting payload clear it's SD Card" << std::endl;
  writeToPayload("lllrrr");
  checkAcknowledge(aggregateResponse(1000,2),'l');
  if(checkAcknowledge(aggregateResponse(30000,2),'l')) {
      std::cout << "[cmd] Payload Reporting SD Clear" << std::endl;
  }
}
