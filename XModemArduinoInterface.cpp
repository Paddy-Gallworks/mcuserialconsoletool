#include "XModemArduinoInterface.h"

XModemArduinoInterface::XModemArduinoInterface() {
  for(int i = 0; i < 96; i++) {
    memBuffer[i] = ' ';
  }
}

// Implement the interface;

int XModemArduinoInterface::recvChar(int _timeOut) {
    time_t now = time(nullptr);
    time_t finishTime = (now * 1000) + _timeOut;
    int counter = 0;
    while((time(nullptr) * 1000) < finishTime) {
      char _byte;
      if(readFromPayload(&_byte)) {
        return _byte;
      }
    }
    return -1;
}
void XModemArduinoInterface::sendChar(char _char) {
	writeToPayload(_char);
	/*std::cout << "[send]: " << formatResponse(_char) << std::endl;*/
}
bool XModemArduinoInterface::rDataHandler(unsigned long _packetCount, char * _buff, int _buffLength) {
	std::cout << "[xmd] Packet Received: " << _packetCount << " of length: " << _buffLength << " containing ";
	for(int i = 0; i < _buffLength; i++) { std::cout << _buff[i]; }
	std::cout << std::endl;
	// Decode the buffer from B64?
	unsigned int m = decode_base64((unsigned char *)_buff, _buffLength, memBuffer);
	// Write the data to the file.
	for(int i = 0; i < m; i++) {
 		outputDataFile.write((char*)&memBuffer[i],1);
  }
	outputDataFile.flush(); // TODO Maybe this is required
	//Return True if the data reception was successful. TODO-> How do we check??
  return true;
}
bool XModemArduinoInterface::tDataHandler(unsigned long _packetRequested, char * _buff, int _requestedLength) {
    if(xModemFileBytesToRead == (unsigned long)0) {
      return false; //Return False if the transmission is complete!
    }
    else {
	// Check the file properties.
	//std::cout << "[xmd] Packet Requested: " << _packetRequested << " Contents: ";
	unsigned long bytesToRead = std::min((long)96,xModemFileBytesToRead);
	if(_packetRequested != xModemPacketCounter) { // True only if a packet is being requested for the first time.
	  //Create SPACE padding in memBuffer
	  //for(int i = 0; i < 96; i++) {memBuffer[i] = ' ';}
	  // Read a fresh packet from the data file
	  
	  //std::cout << "[xmd] Reading File: "; 
	  for(int i = 0; i < bytesToRead; i++) {
	    // Read Byte from File
	    char temp;
	    inputDataFile.get(temp);
	    memBuffer[i] = temp;
	    xModemFileBytesToRead--;
	    //std::cout << std::hex << temp;
	  }
		xModemPacketCounter++;
	}
	//Transfer internal buffer to requesting buffer using b64 encode
	for(int i = 0; i < 128; i++) { _buff[i] = '=';}
	unsigned int m = encode_base64(memBuffer, bytesToRead, (unsigned char *) _buff);
	std::cout << std::dec << std::endl;
	std::cout << "[xmd] Packet Requested: " << _packetRequested << " Contents: " << _buff << std::endl; //buff might be null character terminated and so it's entire contents is not displayed here!
	std::cout << "[xmd] Remaining bytes to read: " << xModemFileBytesToRead << std::endl;	
	return true;
    }
 

}
