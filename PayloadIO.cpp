#include "PayloadIO.h"

int setupPort(char* _device, int _baud) {
  std::cout << "Opening COM port..."<< std::endl;
  std::cout << "Port: " << _device << std::endl;
  int deviceNumber;
  deviceNumber = serialOpen(_device,_baud);
  if(deviceNumber > 1) {
    std::cout << "...Port opened" << std::endl;
    std::cout << "Assigning Port to Instructions..." << std::endl;
    return deviceNumber;
  }
  else {
    std::cout << "Could not Open Port" << std::endl;
    return -1;
  }
}

void reportFromCommand(std::string _message){
  std::cout << "[cmd]: " + _message << std::endl;
}

// Define Interfaces for Instructions to Use
void writeToPayload(std::string _message) {
  int bytesWritten = _message.length();
  //iterate through the string
  for(int i = 0; i < bytesWritten; i++) {
    serialPutchar(deviceNumber, _message[i]);
	  /*std::cout << "[write] : " << formatResponse(_message[i]) << std::endl;*/
  }
  std::cout << "[pio] Bytes Written to Payload: " << bytesWritten << ": " << _message << std::endl;
}
void writeToPayload(char _char) {
  serialPutchar(deviceNumber,_char);
}
// Reads Single Byte from Serial Port If Immediatly Available
bool readFromPayload(char * _byte) {
  if(serialDataAvail(deviceNumber)>0) {
    *_byte = serialGetchar(deviceNumber); // Making Use of WiringPi Library
	  //std::cout << "[read] : " << formatResponse(*_byte) << std::endl;
    return true;
  }
  else {
    return false;
  }
}
void flushSerial() {
	serialFlush(deviceNumber);
}

// ------------------------------------
// Interpret Functions
// ------------------------------------

std::string formatResponse(char _byte) {
  if(_byte == 6) { return "<ack>"; }
  else if(_byte == 1) {return "<SOH>";}
  else if(_byte == 21) {return "<NAK>";}
  else if(_byte == 10) {return "<CR>"; }
  else if(_byte == 13) {return "<LR>"; } 
  else if(_byte == 24) {return "<CAN>";}
  else {
	std::string response = {_byte};
	return response;
  }
}

bool checkAcknowledge(payloadResponse _response, char _cmd) {
  if((char)_response.message[0] == _cmd && (int)_response.message[1] == 6) {
    std::cout << "[chk] Acknowledge Correct --PASS--" << std::endl;
    return true;
  }
  else {
    std::cout << "[chk] Acknowledge Missing --FAIL--" << std::endl;
    std::cout << "[chk] Check Error Code: '" << _response.message << "' - " << _response.duration << std::endl;
    return false;
  }
}

/* aggregateResponse
 *
 * This function joins all the characters received from the payload within the timeout or until
 * the max character limit is reached.
 *
 */
payloadResponse aggregateResponse(unsigned long timeout, int maxCharCount) {
  //std::cout << "[agg] Listening for response..." << std::endl;
  char buff[256]; // Length is overkill for now...Files will be a different function.
  int buffIndex = 0;
  bool continueListening = true;
  unsigned long start = clock();
  unsigned long inter = clock();
  std::cout << "[agg] Listening for response, started: " << start << std::endl;
  while(continueListening) {
    usleep(35000); // Program is sensitive to this delay?..
    if(readFromPayload(&buff[buffIndex])) {
      std::cout << "[agg] Byte Received: " << buffIndex << ": 0x" << std::hex << (int)buff[buffIndex] << std::dec;
      std::cout << " => " << formatResponse(buff[buffIndex]) <<  std::endl;
      //Check for LF and CR
      if((int)buff[buffIndex] == 10 || (int)buff[buffIndex] == 13 || (int)buff[buffIndex] == 24){
        //Don't increment buff index
        std::cout << "[agg] char ignored" << std::endl;
      } else if(buffIndex == 0 && (int)buff[buffIndex] == 6) {// Check for leading Acknowledge and ignore.
        std::cout << "[agg] Leading Ack Ignored" << std::endl;
      } else {
        buffIndex++;
      }
      if(buffIndex == maxCharCount) {
        std::cout << "[agg] Char Limit Reached: 0x" << maxCharCount << std::endl;
		std::cout << "[agg] Finished at: " << inter << " taking " << inter - start << std::endl;
        continueListening = false;
      }
    }
    std::cout << std::dec;
    inter = clock();
    if(inter > (start + timeout)) {
      std::cout << "[agg] Time Out Tripped at " << inter << std::endl;
      continueListening = false;
    }
  }
  payloadResponse response;
  for(int i = 0; i < maxCharCount; i++) {
    response.message += buff[i];
  }
  response.duration = inter - start;
  return response;
}
