#include <iostream>
#include <string>
#include "base64.h"

#ifndef COMMUNICATION_MANAGER_H
#define COMMUNICATION_MANAGER_H

// ----------------------------------------
// COMMUNICATION MANAGER CONFIGURATION
// ----------------------------------------

using namespace std; // For string

#define SOFTWARE_VERSION "0.01"

// ALPHABET VERSION
#define INSTRUCTION_SET_SIZE 26 //lower case alphabet only
// UTILITY FAILURE CODES
#define COMMAND_NOT_RECOGNIZED 48 // ASCII 0
#define COMMAND_DEFINITION_OUT_OF_BOUNDS 49 // ASCII 1
#define SECOND_BYTE_TIME_OUT 50 // ASCII 2
#define TRIPLICATE_MISMATCH 51 // ASCII 3
// COMMUNICATION COSTANTS
const char acknowledge = 6; //ASCII Value for ACK
const char negative_acknowledge = 21; //ASCII Value for NAK
const char line_feed = 10;
const char carridge_feed = 13;
// MODE CONSTATS
#define VERBOSE_MODE 0
#define PACKET_LENGTH 96
#define ENCODED_PACKET_LENGTH 128

class LazyDragonflyCommandInterface {
private:
  std::string id; // Stores the command identifier
public:
  LazyDragonflyCommandInterface(std::string _id) {
    id = _id;
  }
  virtual std::string toString() = 0; // Shares a description of the command
  virtual void execute() = 0; // Performs specific functions of the command
  virtual std::string getID() {
    return id;
  }
};
/* TODO Remove
class LazyDragonflyHardwareInterface {
public:
  virtual char read() = 0;
  virtual bool available() = 0;
  virtual char peek() = 0;
  virtual void sendErrorMessage(string) = 0;
  virtual void write(char) = 0;
  virtual void print(string) = 0;
  virtual void println(string) = 0;
}; */

class LazyDragonfly {
//data
public:
protected:
  //LazyDragonflyHardwareInterface* hardware;
  // Command Interface Declarations
  LazyDragonflyCommandInterface* commandList[INSTRUCTION_SET_SIZE]; // An array of pointers to possible commands.
  int commandCounter = 0;
  /* Previous Function Pointer Implementations
  void (*cmdFunc[INSTRUCTION_SET_SIZE])(char);
  char iden[INSTRUCTION_SET_SIZE];
  uint8_t npara[INSTRUCTION_SET_SIZE]; */
  string buffer;
    //unsigned char buff[3]; //Triplicate buffer
    //unsigned char que[2];
    //uint8_t que_index = 0;
  //bool command_recognized;
  //bool command_carry_over;
  int8_t index_of_command; //TODO get rid of this?
  unsigned char carryOverCmd;
  uint16_t reset_timer = 0;
  bool timeResetMode = false;
  uint16_t reset_time_limit = 10000;
  // Operating Modes
  bool verbose = false;
  bool b64Mode = false;
public:
// Constructor
  LazyDragonfly(); //
  LazyDragonfly(bool, bool); //Setup with modes
// Interface Initialization
//  void setInterface(LazyDragonflyHardwareInterface*);
// Functions
  int run(std::string); // New style string based instructions.
  void addCommand(LazyDragonflyCommandInterface*);
  void setVerbose(bool);
  void setTimeResetMode(bool); // TODO: Not yet implemented
  void setResetTimeOut(int);
  void setB64Mode(bool); //Change Data Modes
  void transmitDataSerial(char *, int); //Transmit Data Generic Function
  //void sendErrorCode(char _buff, unsigned char errorCode);
protected:
  //void sendAcknowledgeMessage(char, string);
  int getCommandIndex(std::string);
  void callWithReset(int);
  void callWithReset(int, char _arg); // TODO: Remove when command interface is complete
  void resetState(); // Clears all que and buffers
  void checkTimeOut();
  void clearTimeOut();
  // Protected Data Transmit Functions
  void transmitThroughMode(char *, int); // Send data straight onto serial bus.
  void transmit64Encode(char *, int); //Send a b64 encoded string onto the serial port.
};

#endif // COMMUNICATION_MANAGER_H
