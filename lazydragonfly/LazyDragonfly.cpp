#include "LazyDragonfly.h"

//
// - LazyDragonfly Object Constructor
//

LazyDragonfly::LazyDragonfly() {
  for(int index = 0; index < INSTRUCTION_SET_SIZE; index++) {
    commandList[index] = NULL;
  }
}

LazyDragonfly::LazyDragonfly(bool _verbose, bool _b64Mode) {
  verbose = _verbose;
  b64Mode = _b64Mode;
  LazyDragonfly();
}

//TODO - This should not be required anymore??
/*
void LazyDragonfly::setInterface(LazyDragonflyHardwareInterface* _hardware) {
  hardware = _hardware;
}*/

//
// - run() - Check If command exists and execute it
//
int LazyDragonfly::run(string _cmdID) {
  int8_t index = getCommandIndex(_cmdID);
  if(index >= 0) {
    callWithReset(index);
  }
  else { // index < 0
    //sendErrorCode(que[0], COMMAND_NOT_RECOGNIZED);
    resetState();
    return COMMAND_NOT_RECOGNIZED; // TODO Make this an object return
  }
  return -1;
}
void LazyDragonfly::addCommand(LazyDragonflyCommandInterface *_command) {
  commandList[commandCounter] = _command;
  commandCounter++;
}
void LazyDragonfly::setVerbose(bool _verbose) {
    verbose = _verbose;
}
void LazyDragonfly::setTimeResetMode(bool _timeResetMode) {
  timeResetMode = _timeResetMode;
}
void LazyDragonfly::setResetTimeOut(int _maxTime) {
  reset_time_limit = _maxTime;
}
void LazyDragonfly::setB64Mode(bool _b64Mode) {
  b64Mode = _b64Mode;
}
void LazyDragonfly::transmitDataSerial(char *_buff, int _length) {
  if(b64Mode) {
    transmit64Encode(_buff,_length);
  }
  else {
    transmitThroughMode(_buff,_length);
  }
}
/*void LazyDragonfly::sendErrorCode(char _buff, unsigned char errorCode) {
  if(verbose) {
      hardware->print("[Err]: ");
      hardware->write(errorCode);
      hardware->println("");
  }
  else {
      hardware->write(_buff);
      hardware->write(negative_acknowledge);
      hardware->write(errorCode);
  }
}*/

// -----------------------------------------------------------------------------
// PROTECTED FUNCTIONS
// -----------------------------------------------------------------------------
/*
void LazyDragonfly::sendAcknowledgeMessage(char _buff, string _message) {
  if(verbose) {
    hardware->print("[fly]: " +_message);
    hardware->write(_buff);
    hardware->println("");
  }
  else {
    hardware->write(_buff);
    hardware->write(acknowledge);
  }
}
*/
int LazyDragonfly::getCommandIndex(string _cmdID) {
  for(int index = 0; index < INSTRUCTION_SET_SIZE; index++) {
    if(commandList[index] != NULL) {
      if(_cmdID.compare(commandList[index]->getID()) == 0) {
        return index;
      }
    }
  }
  return -1;
}
void LazyDragonfly::callWithReset(int index) {
  commandList[index]->execute();
  resetState();
}
void LazyDragonfly::resetState() {
  index_of_command = -1;
  //que_index = 0;
  //command_recognized = false;
  //command_carry_over =false;
  reset_timer = 0;
  buffer = "";
}
void LazyDragonfly::checkTimeOut() {
  if(reset_timer > reset_time_limit) {
    resetState();
  }
  reset_timer++;
}
void LazyDragonfly::clearTimeOut() {
  reset_timer = 0;
}
//Protected Data Transmit FUNCTIONS
void LazyDragonfly::transmitThroughMode(char *_buff, int _length) {
  //hardware->print(_buff);
  //or!
  /*
  for(int i = 0; i < _length, i++) {serialPort.print(_bufff[i]);}
  */
}
void LazyDragonfly::transmit64Encode(char *_buff, int _length) {
  //Create Empty Arrays inside scope
  unsigned char dataArray[PACKET_LENGTH];
  unsigned char encodedArray[ENCODED_PACKET_LENGTH];
  //Fill Padding and then over write with valid data.
  for(int i = 0; i < PACKET_LENGTH; i++) {dataArray[i] = ' ';} // Fill in padding
  for(int i = 0; i < min(PACKET_LENGTH,_length); i++) {dataArray[i] = _buff[i];}
  //Encode data and write to serial
  for(int i = 0; i < ENCODED_PACKET_LENGTH; i++) {encodedArray[i] = '=';}
  unsigned int m = encode_base64(dataArray, min(PACKET_LENGTH,_length), encodedArray);
  // for(int i = 0; i < ENCODED_PACKET_LENGTH; i++) {
  //   serialPort.write(encodedArray[i]);
  // }
  //hardware->print((char*)&encodedArray);
  //If there is remaining data, call this function again with remaining buffer
  if(_length > PACKET_LENGTH) {
    transmit64Encode(&_buff[PACKET_LENGTH],_length - PACKET_LENGTH); // Recursively call this function with shorter buffer.
  }
}
