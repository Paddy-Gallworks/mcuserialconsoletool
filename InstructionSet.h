#ifndef INSTRUCTIONSET_H
#define INSTRUCTIONSET_H

#include <fstream>
#include <cstdint>
#include <ctime>
#include "lazydragonfly/LazyDragonfly.h"
#include "lazydragonfly/base64.h"
#include "PayloadIO.h"
#include "XModemArduinoInterface.h"
extern XModemArduinoInterface payloadInterface;
extern XModem xmod;


// ------------------------------------
// Instruction Set - Implementing Command Interface
// ------------------------------------

/* 'none' - "list" */
/* LIST ALL COMMANDS
 * Sends: Nothing to Payload
 *
 */
class listCommandsInstruction: public LazyDragonflyCommandInterface {
public:
   listCommandsInstruction();
   std::string toString();
   void execute();
};

/* 'none' - "flush" */
/* FLUSH ALL SERIAL DATA
 * Sends: Nothing to Payload
 * Scrubs send and recieve serial buffers
 */

class flushInstruction : public LazyDragonflyCommandInterface {
public:
  flushInstruction();
  std::string toString();
  void execute();
};

/* 'hhh' - "heartbeat"  */
/* REQUEST HEARTBEAT
 * Sends: 'hhh' - three characters
 * Expects: 'h<ack><ack>' - three characters
 *
 */
 class heartbeatInstruction: public LazyDragonflyCommandInterface {
 public:
   heartbeatInstruction();
   std::string toString();
   void execute();
 };

 /* 'vvv' - "version" */
 /* REQUEEST PAYLOAD FIRMWARE VERSION
  * Sends: 'vvv' - three characters
  * Expects: 'v<ack> + software version alphanumeric.
  *
  */
  class versionInstruction: public LazyDragonflyCommandInterface {
  public:
    versionInstruction();
    std::string toString();
    void execute();
  };


/* 'ttt' - "telemetry" */
/* REQUEEST PAYLOAD TELEMETRY
 * Sends: 'ttt' - three characters
 * Expects: 't<ack> *telemetry-bytes*' - three characters
 *
 */
class telemetryInstruction: public LazyDragonflyCommandInterface {
public:
    telemetryInstruction();
    std::string toString();
    void execute();
};

/* 'fff' -  "fileRequest" */
/* REQUEST FILE FROM PAYLOAD
 * Sends 'fff' - three characters
 * Expects: 'f<ack>' + Xmodem protocol file receive
 *
 */
class fileRequestInstruction: public LazyDragonflyCommandInterface {
public:
    fileRequestInstruction();
    std::string toString();
    void execute();
};


/* 'aaa' - "fileSend" */
/* SEND FILE TO PAYLOAD
 * Send 'aaa'
 * Expects: 'a<ack>' + Send XModem
 *
 */
class fileSendInstruction : public LazyDragonflyCommandInterface {
public:
    fileSendInstruction();
    std::string toString();
    void execute();
};

/* 'ccc' - "captureImage" */
/* SEND FILE TO PAYLOAD
 * Send 'ccc'
 * Expects: 'c<ack> + c<ack>'
 *
 */
 class captureImageInstruction : public LazyDragonflyCommandInterface {
 public:
     captureImageInstruction();
     std::string toString();
     void execute();
 };

/* 'ggg' - "ackDown"
 * ACKNOWLEDGE IMAGE DOWNLINK
 * Sends 'ggg'
 * Expects: Acknowledges only
 */
 class ackDownInstruction : public LazyDragonflyCommandInterface {
 public:
     ackDownInstruction();
     std::string toString();
     void execute();
 };

/* 'kkk' - "checkFlags" */
/* CHECK SYSTEM FLAGS ON PAYLOAD
 * Semds 'kkk' SubCodes
 * Expects: Single Bytes for Each SubCode
 *
 */
class checkFlagsInstruction : public LazyDragonflyCommandInterface {
public:
	checkFlagsInstruction();
	std::string toString();
	void execute();
};

/* 'lllhhh' - "sendLog"
 * SEND LOG FILE TO OBC
 * Sends lllhhh
 * Expects: Xmodem transfer
 */

class sendLogInstruction : public LazyDragonflyCommandInterface {
public:
	sendLogInstruction();
	std::string toString();
	void execute();
};

/* 'llliii' - "trimLog"
 * MAINTAIN LOG FILE
 * Sends llliii
 * Expects: acknowledges only
 */

class trimLogInstruction : public LazyDragonflyCommandInterface {
public:
	trimLogInstruction();
	std::string toString();
	void execute();
};

/* 'lllnnn' - "sendDir"
 * SEND DIRECTORY FILE
 * Sends lllnnn
 * Expects: XMod file sent from payload
 */

class sendDirInstruction : public LazyDragonflyCommandInterface {
public:
	sendDirInstruction();
	std::string toString();
	void execute();
};

/* 'lllrrr' - "clearSD"
 * CLEAR SD CARD CONTENTS
 * Sends lllrrr
 * Expects: single ack on completion
 */

class clearSDInstruction : public LazyDragonflyCommandInterface {
public:
	clearSDInstruction();
	std::string toString();
	void execute();
};

#endif // INSTRUCTIONSET_H
