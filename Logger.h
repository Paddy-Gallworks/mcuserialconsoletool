
#include <string>
#include <fstream>

extern std::string sendDirLogFile;
extern std::string fileSendLogFile;
extern std::string heartbeatLogFile;
extern std::string telemetryLogFile;
extern std::string captureLogFilemake ;

#ifndef LOG_FILENAMES
  #define LOG_FILENAMES
  // Logging Filenames
  
  // Logging Global functions
  void appendToFile(std::string _filename, std::string _message);
#endif //LOG_FILENAMES