#include <iostream>
#include <fstream>
#include <iostream>
#include <string>
#include <unistd.h>
#include <time.h>

// Custom Includes
#include "lazydragonfly/LazyDragonfly.h" // True Header
//#include "ConsoleInterface.h" // True Header -> Extends LazyDragonflyHardwareInterface
#include "Serial/wiringSerial.h" // To Setup Port
#include "PayloadIO.h" // For Low Level payload IO
#include "xmodem/XModem.h" // To Create the objects below
#include "XModemArduinoInterface.h" // True Header -> Extends the XModemHardwareInterface
// Global Declarations for XModem Interface.
XModem xmod;
XModemArduinoInterface payloadInterface;
#include "InstructionSet.h" // Multiclass Header: Contains all definitions of LazyDragonflyCommandInterface

using namespace std;

#define PAYLOAD_BAUD 57600
int deviceNumber = 0;

int main(int argc, char** argv)
{
  bool useScheduler = false;
  // Splash Screen (TODO)
  cout << " -- PAYLOAD TESTING CONSOLE -- " << endl;
  // Check Arguments
  if(argc == 2) {
    //TODO: Only a port was provided.
    useScheduler = false;
  }
  else if(argc == 3) {
    //TODO: A port and a scheduler file has been added.
    cout << "Using SCHEDULE FILE: " << argv[2] << endl;
    useScheduler = true;
  }
  else {
    //TODO: Print Usage of function
    cout << "USAGE: " << endl;
    cout << "./payloadConsole <port-name> <[optional]schedule-filename>" << endl;
    return 0;
  }
  cout << "Setting Up Command Handler..." << endl;
  LazyDragonfly cmd;
//  ConsoleInterface conInterface;
  cmd.setVerbose(true);
//  cmd.setInterface(&conInterface);
  cout << "...Command Handler Up" << endl << endl;
  cout << "Setting Up Instruction Set..." << endl;
  deviceNumber = setupPort(argv[1],PAYLOAD_BAUD);
  // Define the Commands
  listCommandsInstruction list;
  heartbeatInstruction heartbeat;
  telemetryInstruction telemetry;
  versionInstruction version;
  fileRequestInstruction fileRequest;
  fileSendInstruction fileSend;
  captureImageInstruction captureImage;
  flushInstruction flushSerial;
  checkFlagsInstruction checkFlags;
  sendLogInstruction sendLog;
  trimLogInstruction trimLog;
  sendDirInstruction sendDir;
  ackDownInstruction ackDown;
  clearSDInstruction clearSD;
  // Add Commands
  cmd.addCommand(&list);
  cmd.addCommand(&heartbeat);
  cmd.addCommand(&telemetry);
  cmd.addCommand(&fileRequest);
  cmd.addCommand(&fileSend);
  cmd.addCommand(&captureImage);
  cmd.addCommand(&flushSerial);
  cmd.addCommand(&checkFlags);
  cmd.addCommand(&sendLog);
  cmd.addCommand(&trimLog);
  cmd.addCommand(&sendDir);
  cmd.addCommand(&ackDown);
  cmd.addCommand(&version);
  cmd.addCommand(&clearSD);
  cout << "...Instructions defined" << endl;
  // Setup XModem Interface
  cout << "Setting XModem..." << endl;
  xmod.begin(&payloadInterface);
  cout << "...XModem Up" << endl;
  // User Input Loop
  bool programContinue = true;
  string userInput = "";
  int sleepTime = 0;
  if(useScheduler) {
	sleep(2);
    // Check that the file can be opened.
    ifstream scheduleFile;
    scheduleFile.open(argv[2]);
    if(scheduleFile.is_open()) {
      while(programContinue) {
        scheduleFile >> userInput;
        scheduleFile >> sleepTime;
		cout << "READING COMMAND FROM FILE: " << userInput << endl;
        if(scheduleFile.eof()) {
          programContinue == false;
		  break;
        }
        else {
          cmd.run(userInput);
          sleep(sleepTime);
        }
        userInput = "";
        sleepTime = 0;
      }
      scheduleFile.close();
    }
    else {
      cout << "Cannot open schedule file. Please check name and retry." << endl;
      scheduleFile.close();
      return -1;
    }
  }
  programContinue = true;
  while(programContinue) {
    // Read Input from User
    cout << "pptc>> ";
    cin >> userInput;
    // Interpret Command
    cmd.run(userInput); // Result of adapting dragonfly to strings.
  }
  return 0;
}
